# Outline dokumen:

- [Kembali ke Readme](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/README.md)
- [Bab 1 Pendahuluan](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_1/bab1.md)
- [Bab 2 Sekilas kondisi riset dan publikasi saat ini](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_2/bab2.md)
- [Bab 3 Konsep sainsterbuka](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_3/bab3.md)
- [Bab 4 Implementasi sainsterbuka](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_4/bab4.md)
- [Bab 5 Contoh kasus](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_5/bab5.md)
- [Bab 6 Penutup](https://gitlab.com/derwinirawan/panduan-sainsterbuka/tree/master/Bab_6)
- [Bab 7 Referensi](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_7/bab7.md)

# Konteks #

Dalam bab 2 dijelaskan kondisi saintifik saat ini, di dunia dan di Indonesia dengan rincian:

- bagaimana regulasi tertulis atau perjanjian tak tertulis (konvensi) yang disepakati
- bagaimana respon kalangan saintifik 
- apa dampak yang dirasakan 

# Lansekap saintifik internasional

Saat kami menggunakan kata kunci "scientific publishing", Google menangkap 2.440.000, dokumen/tautan sedangkan bila kami menggunakan kata kunci "scientific landscape", Google menangkap 49.400 dokumen/tautan. Ini jelas jumlah yang sangat besar. Oleh karena itu, kami akan menyampaikan beberapa hal berikut ini.

- Blog: Ada banyak blog yang mendedikasikan diri untuk membahas lansekap saintifik internasional, berikut ini hanya beberapa diantaranya:

  - [Fossilsandshit](http://fossilsandshit.com/)
  - LSE blog

    - [Conflicting academic attitudes to copyright are slowing the move to open access](http://blogs.lse.ac.uk/impactofsocialsciences/2018/05/10/conflicting-academic-attitudes-to-copyright-are-slowing-the-move-to-open-access/)
    - [Taking back control: the new university and academic presses that are re-envisioning scholarly publishing](http://blogs.lse.ac.uk/impactofsocialsciences/2017/09/20/taking-back-control-the-new-university-and-academic-presses-that-are-re-envisioning-scholarly-publishing/)
  - Science
    - [Internationalizing Japan's scientific landscape](http://www.sciencemag.org/features/2011/09/internationalizing-japans-scientific-landscape)
  - The Guardian
    - [Is the staggeringly profitable business of scientific publishing bad for science?](https://www.theguardian.com/science/2017/jun/27/profitable-business-scientific-publishing-bad-for-science)
  - NatureNews. Berikut ini beberapa contoh artikel yang relevan:
    - [Remapping the scientific landscape: moving from a closed to open science world](http://blogs.nature.com/naturejobs/2017/10/16/remapping-the-scientific-landscape-moving-from-a-closed-to-open-science-world/)

- Artikel terulas (*peer-reviewed article*): berikut ini hanya sebagian kecil yang dapat kami sampaikan dalam waktu yang pendek. Pada kesempatan berikutnya, kami akan mengulas topik ini dengan cara memvisualisasikan data sitasi dari basis data ilmiah.

  - [Mapping the ‘dynamic capabilities’ scientific landscape, 1990-2015: A bibliometric analysis](https://www.tandfonline.com/doi/abs/10.1080/09737766.2017.1306181)
  - [Synthetic Biology: Mapping the Scientific Landscape](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0034368)
  - [The emerging landscape of scientific publishing](The emerging landscape of scientific publishing)
  - dan banyak lainnya

- Laporan
  - [The evolving preprint landscape: introductory report for the Knowledge Exchange Working Group on Preprints](http://apo.org.au/node/172981)


# Lansekap saintifik Indonesia

Google menangkap 96 dokumen atau tautan dengan kata kunci "dunia publikasi Indonesia" dan 2070 dokumen/tautan dengan kata kunci "dunia penelitian Indonesia". Keduanya dilakukan pada tanggal 19 Juni 2018, pukul 04.00. Berikut ini beberapa artikel yang pernah dirilis diantara banyak artikel lainnya tentang lansekap dunia saintifik di Indonesia. 

- Blog

  - nasional

    - [Indonesia ingin jadi No. 1 di ASEAN, tapi dalam dunia ilmu pengetahuan kolaborasi lebih penting](https://theconversation.com/indonesia-ingin-jadi-no-1-di-asean-tapi-dalam-dunia-ilmu-pengetahuan-kolaborasi-lebih-penting-83984), 14 September 2017, oleh: Dicky Pelupessy, media: The Conversation Indonesia,
    - [Kondisi Dunia Penelitian di Indonesia](https://tirto.id/kondisi-dunia-penelitian-di-indonesia-cvvj), 29 Agustus 2017, oleh Scholastica Gerintya, media: Tirto.id
    - [Melesat 1.567 persen, publikasi ilmiah Indonesia di atas rata-rata dunia](https://www.antaranews.com/berita/659063/melesat-1567-persen-publikasi-ilmiah-indonesia-di-atas-rata-rata-dunia), 17 Oktober 2017, oleh: Virna P. Setyorini, media: AntaraNews 
    - [Muramnya wajah dunia riset Indonesia](https://tirto.id/muramnya-wajah-dunia-riset-indonesia-bsF6), 11 Juli 2016, oleh: Nurul Qomariyah Pramisti, media: Tirto.id
    - [Indonesia Tak Lagi Seksi buat Peneliti Asing](https://tirto.id/indonesia-tak-lagi-seksi-buat-peneliti-asing-cFdg), 23 Februari 2018, oleh: Fadrik Aziz Firdausi, media: Tirto.id
    - [Dana Riset Indonesia Paling Rendah di Asia Tenggara](https://tirto.id/dana-riset-indonesia-paling-rendah-di-asia-tenggara-chUP), 30 Januari 2017, oleh: Mutaya Saroh, media: Tirto.id 

  - internasional

    - [Guest post: Publishing in Indonesia — some facts that you might have missed](https://www.authoraid.info/es/noticias/details/1302/), 14 Juni 2018, oleh: Dasapta Erwin Irawan, media: AuthorAid blog.
    - [Pre-print server for Indonesian research](https://www.irtiqa-blog.com/2018/06/pre-print-server-for-indonesian-research.html), 8 Juni 2018, oleh: Salman Hameed, media: personal blog.
    - [Open Science in Indonesia](https://blogs.openaire.eu/?p=3105), 22 Mei 2018, oleh: Lisa Matthias, media: OpenAire blog.
    - [Indonesian plan to clamp down on foreign scientists draws protest](https://www.nature.com/articles/d41586-018-05001-7), 22 Mei 2018, oleh Dyna Rochmyaningsih, media: NatureNews.
    - [Preprint Repositories Gain in Institutional Legitimacy and Recognition, Reduce the Attractiveness of Subscription Journals](https://openscience.com/preprint-repositories-gain-in-institutional-legitimacy-and-recognition-reduce-the-attractiveness-of-subscription-journals/), 7 Januari 2018, oleh: Paolo Markin, media: Openscience.com blog.
    - [Indonesian scientists embrace preprint server](https://www.nature.com/articles/d41586-017-08838-6), 5 Januari 2018, oleh: Ivy Shih, media: NatureNews.

- Artikel terulas (*peer-reviewed article*)

  - [Lecturers' Understanding on Indexing Databases of SINTA, DOAJ, Google Scholar, SCOPUS, and Web of Science: A Study of Indonesians](http://iopscience.iop.org/article/10.1088/1742-6596/954/1/012026/meta)
  - [Penerapan Open Science di Indonesia agar riset lebih terbuka, mudah Diakses, dan Meningkatkan Dampak Saintifik](https://journal.ugm.ac.id/bip/article/view/17054)
  - akan ditambahkan.

# Beberapa regulasi tentang riset dan publikasi di Indonesia

- [Rencana Induk Riset Nasional 2017-2045](http://simlitabmas.ristekdikti.go.id/unduh_berkas/RENCANA%20INDUK%20RISET%20NASIONAL%20TAHUN%202017-2045%20%20-%20Edisi%2028%20Pebruari%202017.pdf) 
- [Panduan penilaian kinerja penelitian Perguruan Tinggi 2013](http://simlitabmas.ristekdikti.go.id/unduh_berkas/Panduan%20Penilaian%20Kinerja%20Penelitian%202013.pdf)
- [Panduan operator kinerja kelembagaan penelitian 2017](http://simlitabmas.ristekdikti.go.id/unduh_berkas/Panduan_Operator_Kinerja_Kelembagaan_Penelitian_2017.pdf)
- [Pedoman publikasi ilmiah 2017](https://drive.google.com/file/d/0B7zXDjeLjpYPaUtlSk81cE5mZ2s/view) ([ulasan dari kami](https://www.authorea.com/189560/wtaxIsuraB7KuyE0FKzbDA))

# Beberapa catatan

## Bagaimana regulasi tertulis atau perjanjian tak tertulis (konvensi) yang disepakati

1. Regulasi umumnya lebih fokus mengatur metrik dan pemeringkatan,
2. Indeksasi menjadi penentu kualitas,
3. Publikasi bukan lagi yang utama, tetapi di mana ia terbit dan berapa Faktor Dampaknya (_Impact Factor_) atau Quartile-nya.

## Bagaimana respon kalangan saintifik 

1. Berlomba mengejar jumlah dengan cara menerbitkan artikel di media yang relatif instan, misal: seminar internasional. Tidak sepenuhnya salah memang, tetapi ini kemudian berimbas kepada sangat banyaknya penyelenggaraan seminar internasional terindeks lembaga tertentu, yang mennyebabkan penurunan kualitas.
2. Diabaikannya [integritas saintifik](https://www.nsf.gov/bfa/dias/policy/si/index.jsp).
3. Mulai melakukan praktek-praktek untuk meningkatkan profil metrik pribadi atau afiliasi.

## Apa dampak yang dirasakan

1. Metrik menjadi tujuan, selanjutnya marak praktek-praktek seperti sitasi berlebihan ke karya sendiri, plagiarisme dan self-plagiarisme.
2. Motif finansial menjadi mengemuka. Ini karena interpretasi yang salah tentang akses terbuka (OA, *open access*). Di sisi penulis muncul keluhan bahwa biaya publikasi OA sangat tinggi. Institusi kemudian berupaya secara sporadis membiayai biaya publikasi para dosennya, tanpa pertimbangan yang matang.
3. Jurnal predatori bermunculan, menunggu para dosen/peneliti yang salah pilih media.

## Beberapa pergeseran

1. Munculnya jurnal-jurnal akses terbuka (OA)
2. Munculnya jurnal-jurnal OA tanpa APC (diamond OA)
3. Praktek sains terbuka mulai banyak diadopsi
4. Menjamurnya proyek-proyek kolaboratif di platform [Github](github.com), [Gitlab](gitlab.com), Bitbucket, dll.
5. Preprint menjadi salah satu bentuk luaran riset dan makin banyak pendana riset dan institusi yang mengakuinya.

## Beberapa kontradiksi

1. Dana riset diklaim rendah, tetapi:
  - tidak pernah ada dorongan untuk mengoptimalkan hal dana yang riset tersebut, misal dengan himbauan untuk memilih topik riset berbiaya rendah. 
   - masih berpikiran untuk menerbitkan artikel di jurnal OA dengan biaya APC tinggi dengan alasan metrik.
  - anggaran masih padat untuk barang modal.

2. Data diklaim penting, tetapi:
  - belum ada panduan tentang pengelolaan data riset (_research data management_/RDM).
  - luaran riset yang independen.
  - belum pernah ada keharusan untuk mengunggah data mentah, khuususnya yang dikelola oleh Kemristekdikti melalui platform [Simlitabmas](simlitabmas.ristekdikti.go.id).

3. Jurnal terbitan dalam negeri sangat banyak (no dua di [DOAJ](doaj.org), hingga Mei 2018), tetapi:

   - jurnal dalam negeri, walaupun telah terakreditasi, posisinya (peringkatnya) tetap lebih rendah dibanding jurnal terbitan luar negeri.
   - menggunakan standar kualitas yang hampir tidak dapat dipenuhi oleh mayoritas jurnal dalam negeri.
   - dorongan (secara implisit) untuk mengirimkan artikel ke jurnal luar negeri tetap lebih kuat dibandingkan dorongan untuk mengirimkannya ke jurnal dalam negeri.  

4. Semua pihak ingin memajukan Bahasa Indonesia, tetapi:

   - penghargaan lebih tinggi diberikan kepada jurnal yang dioperasikan dalam Bahasa Inggris dan didedikasikan untuk memuat artikel dalam Bahasa Inggris dan mengundang penulis dari luar Indonesia.
   - artikel dalam Bahasa Inggris lebih dihargai dibandingkan artikel yang ditulis dalam Bahasa Indonesia ([baca juga ulasan ini](https://www.authorea.com/212352/lthkCAgxV9zH6j3BI7femQ)).
   - mempersepsikan bahwa material yang ditulis dalam Bahasa Indonesia tidak dapat atau tidak akan menerima perhatian dari dunia internasional.    


# Outline dokumen:

- [Kembali ke Readme](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/README.md)
- [Bab 1 Pendahuluan](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_1/bab1.md)
- [Bab 2 Sekilas kondisi riset dan publikasi saat ini](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_2/bab2.md)
- [Bab 3 Konsep sainsterbuka](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_3/bab3.md)
- [Bab 4 Implementasi sainsterbuka](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_4/bab4.md)
- [Bab 5 Contoh kasus](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_5/bab5.md)
- [Bab 6 Penutup](https://gitlab.com/derwinirawan/panduan-sainsterbuka/tree/master/Bab_6)
- [Bab 7 Referensi](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_7/bab7.md)
