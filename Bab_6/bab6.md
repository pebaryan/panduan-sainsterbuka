
Penutup
===

# Tujuh hal yang bukan merupakan semangat sainsterbuka #

![open without doors](open.png)

1. **Sainsterbuka adalah cabang baru dari sains**: Sainsterbuka bukan cabang sains yang baru, tetapi adalah cara kita mengerjakan sains.
2. **Sainsterbuka tidak mengakui proses *peer-review* yang saat ini menjadi standar pengujian validitas riset**: Sainsterbuka masih sangat mengakui proses *peer-review*, kami hanya memberikan sentuhan agar lebih terbuka, transparan, dan inklusif berdasarkan standar-standar yang disepakati bersama. Satu hal yang kami coba promosikan adalah, bahwa proses *peer-review (PR)* tidak perlu harus dikendalikan oleh jurnal. Proses PR bisa dikendalikan oleh komunitas, bahkan mungkin tanpa koordinasi sama sekali. PR dari personil acak (*random person*) bisa saja memberikan nilai lebih dibandingkan proser PR formal, yang dalam persepsi saat ini selalu di bawah kendali jurnal atau komite seminar.
3. **Sainsterbuka anti kemapanan**: Sebenarnya bukan itu yang kami cari. Tapi kalau definisi kemapanan adalah kenyamanan karena telah berada pada puncak tertinggi dalam klasemen metrik, maka itu yang kami coba luruskan. Kami menyadari tidak ada metrik yang sempurna, karena itu, kami mempromosikan penggunaan metrik secara bertanggungjawab dan pada lingkup yang sesuai. Satu hal yang kami tidak setujui adalah menggunakan metrik apapun untuk menilai kualitas suatu karya ataupun kepakaran seseorang. 
4. **Sainsterbuka sulit dilakukan**: Sulit di sini perlu dirinci kembali di bagian mana. Tapi kami mengakui, cara-cara yang dipromosikan sainsterbuka, banyak yang tidak sama dengan standar yang saat ini diakui banyak orang. Tapi kami dapat yakinkan bahwa cara kami lebih berkelanjutan (*sustainable*). Kenapa? Karena lebih terbuka dan transparan. Sering kita nyatakan bahwa sains memerlukan keterbukaan dan transparansi, namun pada saat yang sama kita sendiri yang membangun tembok tinggi dan tebal, sehingga untuk mencapainya, proses PR secara tertutup misalnya. Hanya untuk satu proses ini, PR, bagaimana kita dapat membangun persepsi yang sama bila komunikasi antara penulis dan  pengulas (*peer-reviewer*) berjalan searah dan tidak ada komunikasi verbal yang terjalin, karena sistem *blind review* atau *double blind review*.
5. **Sainsterbuka akan memudahkan seseorang mencuri ide kita**: Justru sebaliknya. Sainsterbuka akan merekam jejak digital Anda. Bila jejak digital telah terekam, maka siapapun yang menggunakan karya kita tanpa atribusi yang cukup, dapat dideteksi pula dengan cepat. Jadi sainsterbuka justru memberikan Anda "amunisi" untuk menangkal pencurian tersebut.
6. **Sainsterbuka mahal**: Bisa dijelaskan bagian mana? Apakah bagian di mana kami mendorong untuk membuka akses terhadap dokumen Anda (*open access*)? Justru kami meluruskan, bahwa untuk membuat dokumen Anda terbuka, tidak perlu biaya besar, bahkan seringkali nol Rupiah. Ada banyak cara, dan sebagian besar, yang diperlukan hanya niat, waktu, dan akses internet.
7. **Sainsterbuka tidak diakui dunia**: Tidak juga. Yang saat ini terekspos adalah standar-standar yang ditentukan oleh sebagian orang saja. Sebagian yang lain lebih memilih untuk membuka diri dan berjejaring tanpa standar-standar yang sering muncul dalam perbincangan.

`Bahwa bila saat ini ada aturan, kita harus taati dan ikuti. Tapi dengan pemikiran suatu saat harus diubah. Nah kalau kitanya sendiri tidak pernah mempertanyakan, maka siapa yang akan mengubahnya.`

![seven](seven.jpg)
