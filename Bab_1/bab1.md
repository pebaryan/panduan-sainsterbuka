BAB 1 PENDAHULUAN
---

# Outline dokumen:

- [Kembali ke Readme](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/README.md)
- [Bab 1 Pendahuluan](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_1/bab1.md)
- [Bab 2 Sekilas kondisi riset dan publikasi saat ini](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_2/bab2.md)
- [Bab 3 Konsep sainsterbuka](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_3/bab3.md)
- [Bab 4 Implementasi sainsterbuka](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_4/bab4.md)
- [Bab 5 Contoh kasus](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_5/bab5.md)
- [Bab 6 Penutup](https://gitlab.com/derwinirawan/panduan-sainsterbuka/tree/master/Bab_6)
- [Bab 7 Referensi](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_7/bab7.md)

# Terminologi sains terbuka

Berikut ini adalah beberapa definisi sains terbuka (open science).

Menurut
[FOSTER Science](https://www.fosteropenscience.eu/foster-taxonomy/open-science-definition):

"_Open Science is the practice of science in such a way that others
can collaborate and contribute, where research data, lab notes and
other research processes are freely available, under terms that enable
reuse, redistribution and reproduction of the research and its
underlying data and methods_".

"Sains terbuka adalah praktek sains yang memungkinkan kolaborasi dan
kontribusi dari banyak pihak, di mana data riset, catatan laboratorium
dan proses riset lainnya tersedia secara bebas, yang secara
keseluruhan (termasuk data dan metode) dapat digunakan ulang,
didistribusikan ulan, dan direproduksi".

Menurut
[Wikipedia/open science](https://en.wikipedia.org/wiki/Open_science):

"*Open science is the movement to make scientific research, data and*
*dissemination accessible to all levels of an inquiring society,*
*amateur or professional. It encompasses practices such as publishing*
*open research, campaigning for open access, encouraging scientists
to* *practice open notebook science, and generally making it easier
to* *publish and communicate scientific knowledge*".

"Sains terbuka adalah suatu gerakan untuk membuat riset saintifik,
data dan penyebarannya dapat diakses oleh semua komunitas yang
membutuhkannya, pada semua tingkatan, amatir atau profesional. Ia
mencakup praktek seperti: mempublikasikan riset terbuka, kampanye
akses terbuka, yang mendorong ilmuan untuk mempraktekkan catatan riset
terbuka, dan membuat sains lebih mudah dipublikasikan dan
dikomunikasikan".

Menurut
[The Organisation for Economic Co-operation and Development (OECD)](http://www.oecd.org/science/open-science.htm):

"*Open science encompasses unhindered access to scientific articles,
access to data from public research, and collaborative research
enabled by ICT tools and incentives*".

"Sains terbuka mencakup akses tak terbatas pada artikel saintifik,
akses untuk data dan riset publik, dan riset kolaboratif yang
dimungkinkan oleh perangkat ICT dan insentif".

# Tujuan sains terbuka

Tujuan sains terbuka dijelaskan dengan sangat baik oleh beberapa
inisiatif berikut ini.

Menurut [OECD](http://www.oecd.org/science/open-science.htm):

- *Open science promotes a more accurate verification of scientific
  results. By combining the tools of science and information
  technologies, scientific enquiry and discovery can be sped up for
  the benefit of society* (sains terbuka mempromosikan verifikasi
  saintifik yang lebih akurat).
- *Open science reduces duplication in collecting, creating,*
*transferring and re-using scientific material* (sains terbuka
mengurangi potensi duplikasi dalam pengumpulan, pembuatan, pemindahan,
dan penggunaan ulang material saintifik).
- *Open science increases productivity in an era of tight budgets*
  (sains terbuka meningkatkan produktivitas dengan biaya rendah).
- *Open science promotes citizens’ trust in science. Greater citizen*
*engagement leads to active participation in scientific experiments*
*and data collection* (sains terbuka mempromosikan kepercayaan
masyarakat kepada sains. Komunikasi yang meningkat dengan masyarakat
akan mengarah kepada peningkatan partisipasi dalam eksperimen
saintifik dan pengumpulan data).
- *Open science results in great innovation potential and increased*
*consumer choice from public research* (sains terbuka menghasilkan
potensi penemuan besar dan peningkatan pilihan masyarakat kepada riset
publik).

# Sejarah sains terbuka

# Konsep sains terbuka

Konsep sains terbuka adalah sebagai berikut:

1. melakukan riset secara terbuka sejak awal dengan cara:
  - membuka proposal semaksimum mungkin tanpa melanggar ketentuan dan
    etika saintifik,
  - atau mendaftarkan proposal sebagai *pre-registration*,
  - membuat *open-live report* untuk menjelaskan hasil riset per
    tahap,
2. membuka seluas mungkin potensi kolaborasi dan kontribusi dari pihak
   lain.
3. membuka hasil riset seluas mungkin dengan tujuan untuk:
  - menyebarluaskan data dan informasi kepada masyarakat,
  - mendapatkan berbagai masukan dari masyarakat,
  - mengundang kolaborasi, kontribusi, dan bantuan dari masyarakat
    untuk merinci dan memperdalam hasil riset.
4. menggunakan piranti lunak kode terbuka (*open source*).

# Komponen sains terbuka

## Komponen umum (lengkap)

Berikut ini adalah komponen umum atau komponen lengkap dari sains
terbuka.

Menurut
[FOSTER science](https://www.fosteropenscience.eu/taxonomy/term/102):

![Taksonomi sains terbuka menurut FOSTER](/Users/dasaptaerwinirawan/Documents/2018/Researches/panduan-sainsterbuka/Bab_1_Pendahuluan/foster-taxonomy.png)

Menurut
[Wikipedia/open science](https://en.wikipedia.org/wiki/Open_science):

![Komponen sains terbuka](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9c/Open_Science_-_Prinzipien.png/640px-Open_Science_-_Prinzipien.png)

## Komponen minimum

Komponen minimum menurut kami meliputi tiga komponen. Ketiga komponen
ini sangat diperlukan untuk membuat suatu riset dapat direproduksi
(*reproducible research*):

1. *open data and method* (data dan metode terbuka): data terbuka merupakan komponen yang
   esensial. Metode juga perlu dibuka dan dipaparkan dengan jelas agar
   pengguna dapat memverifikasi hasil riset. 
2. *open repository* (repositori terbuka): tempat penyimpanan yang
   terbuka sangat penting, karena di sinilah pengguna akan menemukan
   dan mengeksplorasi riset Anda. Perlu ditekankan di sini bahwa
   repositori bukanlah jejaring sosial yang mengutamakan jumlah
   pengguna dengan memaksanya untuk registrasi ke dalam
   sistem. Repositori yang dimaksud di sini adalah media yang tidak
   mengharuskan pengguna untuk registrasi.
3. aplikasi *open source* (kode terbuka): aplikasi atau perangkat
   lunak juga dibutuhkan yang bersifat terbuka, bukan berbayar atau
   bergantung kepada sistem monopoli sepihak. Ini sangat penting agar
   pengguna dapat mengulang riset kita dengan sumber daya yang ada
   padanya, misal: bila ia tidak memiliki Ms Office resmi, maka ia
   tidak akan dapat membuka berkas `docx`, `xlsx`, atau `pptx` yang
   Anda buat. Untuk itu format berkas yang Anda gunakan juga perlu
   yang bersifat terbuka.

# Outline dokumen:

- [Kembali ke Readme](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/README.md)
- [Bab 1 Pendahuluan](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_1/bab1.md)
- [Bab 2 Sekilas kondisi riset dan publikasi saat ini](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_2/bab2.md)
- [Bab 3 Konsep sainsterbuka](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_3/bab3.md)
- [Bab 4 Implementasi sainsterbuka](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_4/bab4.md)
- [Bab 5 Contoh kasus](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_5/bab5.md)
- [Bab 6 Penutup](https://gitlab.com/derwinirawan/panduan-sainsterbuka/tree/master/Bab_6)
- [Bab 7 Referensi](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_7/bab7.md)
