Dalam bab ini perlu menjelaskan mengenai:

- terminologi sains terbuka,
- menurut siapa,
- bagaimana konsepnya

Referensi harus ada, karena pada tingkat ini kita akan mengaitkan justifikasi kita tentang pentingnya sains terbuka menurut kita dan juga berbagai sumber.

Untuk referensi, berbagai sumber dari lembaga atau inisiatif internasional dapat digunakan, seperti Bank Dunia, FOSTER, COPE dll.
