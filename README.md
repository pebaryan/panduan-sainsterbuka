Panduan Sainsterbuka
---

`Open science is not a new science, but how we do science`

`Sains terbuka bukan sains yang baru, tapi cara menjalankan sains`

# Tujuan:

Panduan ini dibuat untuk para peneliti dan dosen (P/D) yang berminat
untuk mengimplementasikan konsep sainsterbuka dalam riset atau dalam
alur kerjanya.

# Tim (sesuai abjad):

Tim utama:

	- Afrilya [IGDORE](https://igdore.org/)
	- Dasapta Erwin Irawan, Institut Teknologi Bandung ([ORCID](http://orcid.org/0000-0002-1526-0863))
	- Juneman Abraham, Universitas Bina Nusantara ([ORCID](http://orcid.org/0000-0003-0232-2735))
	- Sami Kandha
	- Surya Darma [IGDORE](https://igdore.org/)

Kolaborator:

	- aaa
	- bbb
	- ccc
	- ddd
	- eee
	- ffff

** Want to join our band wagon? **

Isi [form dukungan ini](https://goo.gl/forms/VQp38F2olbyWFtc33) 

# Misi:

Mempromosikan konsep sainsterbuka ke dalam alur kerja riset yang
memiliki beberapa nilai sebagai berikut:

- terbuka
- transparan
- kolaborasi yang inklusif

# Draft bab

Pada draft pertama ini, kami membuat sebanyak tujuh bab:

- [Bab 1 Pendahuluan](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_1/bab1.md)
- [Bab 2 Sekilas kondisi riset dan publikasi saat ini](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_2/bab2.md)
- [Bab 3 Konsep sainsterbuka](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_3/bab3.md)
- [Bab 4 Implementasi sainsterbuka](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_4/bab4.md)
- [Bab 5 Contoh kasus](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_5/bab5.md)
- [Bab 6 Penutup](https://gitlab.com/derwinirawan/panduan-sainsterbuka/tree/master/Bab_6)
- [Bab 7 Referensi](https://gitlab.com/derwinirawan/panduan-sainsterbuka/blob/master/Bab_7/bab7.md)


